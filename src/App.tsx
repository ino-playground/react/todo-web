import React, { FC } from "react";
import { Home } from "components/pages/";
import "./App.css";

const App: FC = () => <Home />;

export default App;
