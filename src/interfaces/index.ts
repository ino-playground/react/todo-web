export type Task = {
  id: number;
  userId: number;
  title: string;
  content: string;
  isChecked: boolean;
};

export type FilterProps = {
  isFiltered: boolean;
  userId?: number;
};
