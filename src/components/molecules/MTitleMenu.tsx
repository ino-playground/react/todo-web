import React, { FC } from "react";
import { Grid, Button } from "@material-ui/core";
import { makeStyles } from "@material-ui/core/styles";

const useStyles = makeStyles((theme) => ({
  heroButtons: {
    marginTop: theme.spacing(4),
  },
}));

export const MTitleMenu: FC<any> = () => {
  const classes = useStyles();

  return (
    <div className={classes.heroButtons}>
      <Grid container spacing={2} justify="center">
        <Grid item>
          <Button variant="contained" color="primary">
            Sign in
          </Button>
        </Grid>
        <Grid item>
          <Button variant="outlined" color="primary">
            Sign up
          </Button>
        </Grid>
      </Grid>
    </div>
  );
};
