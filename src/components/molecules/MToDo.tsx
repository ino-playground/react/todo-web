import React, { FC } from "react";
import { makeStyles } from "@material-ui/core/styles";
import {
  Avatar,
  Button,
  Card,
  CardContent,
  CardActions,
  Checkbox,
  colors,
  Grid,
  Typography,
  CardHeader,
} from "@material-ui/core";
import { Task } from "interfaces";

const useStyles = makeStyles(() => ({
  avatar: {
    backgroundColor: colors.red[500],
  },
  card: {
    height: "100%",
    display: "flex",
    flexDirection: "column",
  },
  cardMedia: {
    paddingTop: "56.25%", // 16:9
  },
  cardContent: {
    flexGrow: 1,
  },
}));

export const MToDo: FC<Task> = ({ id, userId, title, content, isChecked }) => {
  const classes = useStyles();

  return (
    <Grid item key={id} xs={12} sm={6} md={4}>
      <Card className={classes.card}>
        <CardHeader
          avatar={
            <Avatar aria-label="recipe" className={classes.avatar}>
              {title[0].toUpperCase()}
            </Avatar>
          }
          title={title}
          subheader={`User ID: ${userId}`}
        />
        <CardContent className={classes.cardContent}>
          <Typography>{content}</Typography>
        </CardContent>
        <CardActions>
          <Button size="small" color="primary">
            View
          </Button>
          <Button size="small" color="primary">
            Edit
          </Button>
          <Checkbox
            color="primary"
            checked={isChecked}
            inputProps={{ "aria-label": "secondary checkbox" }}
          />
        </CardActions>
      </Card>
    </Grid>
  );
};
