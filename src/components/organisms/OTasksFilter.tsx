import React, { FC, ChangeEvent } from "react";
import { TextField, FormControlLabel, Switch } from "@material-ui/core";
import { makeStyles } from "@material-ui/core/styles";
import { FilterProps } from "interfaces";

const useStyles = makeStyles((theme) => ({
  tasksfilter: {
    marginTop: theme.spacing(4),
    textAlign: "center",
  },
  filteringSwitch: {
    marginTop: theme.spacing(1),
    paddingRight: theme.spacing(2),
  },
}));

type Props = {
  filterProps: FilterProps;
  onChange: (props: FilterProps) => void;
};

export const OTasksFilter: FC<Props> = ({ filterProps, onChange }) => {
  const classes = useStyles();

  const parseFilterProps = (
    e: ChangeEvent<HTMLTextAreaElement | HTMLInputElement>
  ): FilterProps => {
    const result: FilterProps = {
      isFiltered: filterProps.isFiltered,
    };
    switch (e.target.id) {
      case "checkFiltered":
        result.isFiltered = !filterProps.isFiltered;
        if (filterProps.userId) {
          result.userId = filterProps.userId;
        }
        break;
      case "inputUserId": {
        const value = parseInt(e.target.value, 10);
        if (value <= 0) {
          e.target.value = "";
          break;
        }
        result.userId = value;
        break;
      }
      default:
        break;
    }

    return result;
  };

  return (
    <div className={classes.tasksfilter}>
      <FormControlLabel
        className={classes.filteringSwitch}
        control={
          <Switch
            checked={filterProps.isFiltered}
            onChange={(e) => onChange(parseFilterProps(e))}
            name="checkFiltered"
            color="primary"
            id="checkFiltered"
          />
        }
        label="Filtering"
      />
      <TextField
        id="inputUserId"
        disabled={!filterProps.isFiltered}
        label="User Id"
        type="number"
        InputLabelProps={{ shrink: true }}
        onChange={(e) => onChange(parseFilterProps(e))}
      />
    </div>
  );
};
