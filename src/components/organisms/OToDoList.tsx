import React, { FC, useState, useEffect } from "react";
import axios from "axios";
import { Container, Grid } from "@material-ui/core";
import { makeStyles } from "@material-ui/core/styles";
import { MToDo } from "components/molecules";
import { Task, FilterProps } from "interfaces";

const useStyles = makeStyles((theme) => ({
  cardGrid: {
    paddingTop: theme.spacing(8),
    paddingBottom: theme.spacing(8),
  },
}));

type Props = {
  filterProps: FilterProps;
};

type DataProps = {
  tasks: Task[];
};

const baseUrl = "http://localhost:8080/api/v1";

export const OToDoList: FC<Props> = ({ filterProps }) => {
  const classes = useStyles();

  const [data, setData] = useState<DataProps>({ tasks: [] });

  useEffect(() => {
    const fetchTasks = async () => {
      const result = await axios(
        filterProps.isFiltered
          ? `${baseUrl}/tasks?userId=${filterProps.userId}`
          : `${baseUrl}/tasks`
      );
      setData(result.data);
    };
    fetchTasks();
  }, [filterProps]);

  return (
    <Container className={classes.cardGrid} maxWidth="md">
      <Grid container spacing={4}>
        {data.tasks.map((task) => (
          <MToDo
            key={task.id}
            id={task.id}
            userId={task.userId}
            title={task.title}
            content={task.content}
            isChecked={task.isChecked}
          />
        ))}
      </Grid>
    </Container>
  );
};
