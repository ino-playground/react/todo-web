import React, { FC } from "react";
import { makeStyles } from "@material-ui/core/styles";
import { AppBar, Toolbar, Typography } from "@material-ui/core";
import AppsIcon from "@material-ui/icons/Apps";

const useStyles = makeStyles((theme) => ({
  icon: {
    marginRight: theme.spacing(2),
  },
}));

export const Header: FC = () => {
  const classes = useStyles();

  return (
    <AppBar position="static">
      <Toolbar>
        <AppsIcon className={classes.icon} />
        <Typography variant="h6" color="inherit" noWrap>
          ToDo List...
        </Typography>
      </Toolbar>
    </AppBar>
  );
};
