import React, { FC, useState } from "react";
import { Header } from "components/templates";
import { OHero, OTasksFilter, OToDoList } from "components/organisms";
import { FilterProps } from "interfaces";

export const Home: FC = () => {
  const [filterProps, setFilterProps] = useState<FilterProps>({
    isFiltered: false,
  });

  const handleChange = (props: FilterProps) => {
    setFilterProps(props);
  };

  return (
    <>
      <Header />
      <main>
        <OHero />
        <OTasksFilter filterProps={filterProps} onChange={handleChange} />
        <OToDoList filterProps={filterProps} />
      </main>
    </>
  );
};
